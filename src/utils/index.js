/**
 * Created by PanJiaChen on 16/11/18.
 */
import _ from 'lodash'
/**
 * Parse the time to string
 * @param {(Object|string|number)} time
 * @param {string} cFormat
 * @returns {string}
 */
export function parseTime(time, cFormat) {
  if (arguments.length === 0) {
    return null
  }
  const format = cFormat || '{y}-{m}-{d} {h}:{i}:{s}'
  let date
  if (typeof time === 'object') {
    date = time
  } else {
    if ((typeof time === 'string') && (/^[0-9]+$/.test(time))) {
      time = parseInt(time)
    }
    if ((typeof time === 'number') && (time.toString().length === 10)) {
      time = time * 1000
    }
    date = new Date(time)
  }
  const formatObj = {
    y: date.getFullYear(),
    m: date.getMonth() + 1,
    d: date.getDate(),
    h: date.getHours(),
    i: date.getMinutes(),
    s: date.getSeconds(),
    a: date.getDay()
  }
  const time_str = format.replace(/{(y|m|d|h|i|s|a)+}/g, (result, key) => {
    let value = formatObj[key]
    // Note: getDay() returns 0 on Sunday
    if (key === 'a') { return ['日', '一', '二', '三', '四', '五', '六'][value ] }
    if (result.length > 0 && value < 10) {
      value = '0' + value
    }
    return value || 0
  })
  return time_str
}

/**
 * @param {number} time
 * @param {string} option
 * @returns {string}
 */
export function formatTime(time, option) {
  if (('' + time).length === 10) {
    time = parseInt(time) * 1000
  } else {
    time = +time
  }
  const d = new Date(time)
  const now = Date.now()

  const diff = (now - d) / 1000

  if (diff < 30) {
    return '刚刚'
  } else if (diff < 3600) {
    // less 1 hour
    return Math.ceil(diff / 60) + '分钟前'
  } else if (diff < 3600 * 24) {
    return Math.ceil(diff / 3600) + '小时前'
  } else if (diff < 3600 * 24 * 2) {
    return '1天前'
  }
  if (option) {
    return parseTime(time, option)
  } else {
    return (
      d.getMonth() +
      1 +
      '月' +
      d.getDate() +
      '日' +
      d.getHours() +
      '时' +
      d.getMinutes() +
      '分'
    )
  }
}

/**
 * @param {string} url
 * @returns {Object}
 */
export function param2Obj(url) {
  const search = url.split('?')[1]
  if (!search) {
    return {}
  }
  return JSON.parse(
    '{"' +
      decodeURIComponent(search)
        .replace(/"/g, '\\"')
        .replace(/&/g, '","')
        .replace(/=/g, '":"')
        .replace(/\+/g, ' ') +
      '"}'
  )
}

// 根据 id 生成树状结构
export const treeToList = (array, parent, tree, idField = "id", parentIdfield = "replyTargetId") => {
  tree = typeof tree !== 'undefined' ? tree : []
  parent = typeof parent !== 'undefined' ? parent : {
    id: 0
  }
  _.map(array, function (arr) {
    _.set(arr, 'isParent', true) // 为每个循环项添加 isParent 属性，并赋值为 true
  })
  var children = _.filter(array, function (child) {
    return child[parentIdfield] == parent[idField]
  })

  if (!_.isEmpty(children)) {
    if (parent.id == 0) {
      tree = children
    } else {
      parent['children'] = children
    }
    _.each(children, function (child) {
      treeToList(array, child)
    })
  }

  return tree;
}

// 将评论数据展平
export const flattenArray = (array) => {
  // 将数组中 children 中所有多层级 children 都展开在一个 children 里面
  function pushChildren(arrItem, children) {
    if (arrItem.children && arrItem.children.length > 0) {
      arrItem.children.forEach(item => {
        children.push(item)
        if (item.children) {
          pushChildren(item, children)
        }
      })
    }
  }
  for (let i = 0; i < array.length; i++) {
    pushChildren(array[i], array[i].children)
    // 数组去重
    array[i].children = Array.from(new Set(array[i].children))
    // 删除多余的 children
    array[i].children.forEach(item => {
      delete item.children
    })
  }
}
