import http from "@/utils/http"

// 获取签到阈值
export const checkInThreshold = (params = {}) => {
  return http.get('/api/Configs/CheckInThreshold', params)
}