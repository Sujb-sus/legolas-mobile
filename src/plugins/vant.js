// 按需全局引入 vant组件
import Vue from 'vue'
import {
  Button,
  List,
  Cell,
  Tabbar,
  TabbarItem,
  Swipe,
  SwipeItem,
  Field,
  Uploader,
  Form,
  CellGroup,
  Notify,
  Tab,
  Tabs,
  Popup,
  Overlay,
  Icon,
  Toast,
  Divider,
  CountDown,
  DropdownMenu,
  DropdownItem,
  Lazyload,
  Panel,
  ImagePreview,
  Radio,
  RadioGroup,
  DatetimePicker,
  Progress,

} from 'vant'
Vue.use(Button)
Vue.use(Cell)
Vue.use(List)
Vue.use(Swipe)
Vue.use(SwipeItem)
Vue.use(Field)
Vue.use(Tabbar)
Vue.use(TabbarItem)
Vue.use(Uploader)
Vue.use(Form)
Vue.use(CellGroup)
Vue.use(Notify)
Vue.use(Tab)
Vue.use(Tabs)
Vue.use(Popup)
Vue.use(Overlay)
Vue.use(Icon)
Vue.use(Toast)
Vue.use(Divider)
Vue.use(CountDown)
Vue.use(DropdownMenu)
Vue.use(DropdownItem)
Vue.use(Lazyload)
Vue.use(Panel)
Vue.use(ImagePreview)
Vue.use(Radio)
Vue.use(RadioGroup)
Vue.use(DatetimePicker)
Vue.use(Progress)


Vue.prototype.$notify = Notify
