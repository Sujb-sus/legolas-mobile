import {
  USER_INFO,
  GET_TOKEN,
  USER_STATUS
} from "./modules-type"
import {
  handleLogin,
  userInfo,
  userStatus
} from '@/api/user'
const user = {
  namespaced: true,
  state: {
    user_body: localStorage.getItem('user_body') ? JSON.parse(localStorage.getItem('user_body')) : {},
    user_status: localStorage.getItem('user_status') ? JSON.parse(localStorage.getItem('user_status')) : {},
    token: localStorage.getItem('user_Info') ? JSON.parse(localStorage.getItem('user_Info')).token : '',
    userId: localStorage.getItem('user_Info') ? JSON.parse(localStorage.getItem('user_Info')).userId : '',
  },
  mutations: {
    [GET_TOKEN](state, params) {
      state.token = params.token
      state.userId = params.userId
    },
    [USER_INFO](state, params) {
      state.user_body = params
    },
    [USER_STATUS](state, params) {
      state.user_status = params
    },
  },
  actions: {
    async login({
      commit
    }, paras) {
      const res = await handleLogin(paras)
      if (res && res.code === 200) {
        const data = res.result
        localStorage.setItem('user_Info', JSON.stringify(data))
        console.log(data)
        commit(GET_TOKEN, data)
      }else{
       return this.$toast('网络错误')
      }
      return res
    },
    async userInfo({
      commit
    }) {
      const res = await userInfo()
      if (res && res.code === 200) {
        res.result.gender = res.result.gender.toString()
        const data = res.result
        localStorage.setItem('user_body', JSON.stringify(data))
        commit(USER_INFO, data)
      }else{
        return this.$toast('网络错误')
      }
    },
    async userStatus({
      commit
    }) {
      const res = await userStatus()
      if (res && res.code === 200) {
        const data = res.result
        localStorage.setItem('user_status', JSON.stringify(data))
        commit(USER_STATUS, data)
      }else{
        return this.$toast('网络错误')
      }
    },
  }
}

export default user
