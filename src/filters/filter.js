/**
 *格式化时间
 *yyyy-MM-dd hh:mm:ss
 */
export function formatDate(time, fmt) {
  if (time === undefined || '') {
    return
  }
  const date = new Date(time)
  if (/(y+)/.test(fmt)) {
    fmt = fmt.replace(RegExp.$1, (date.getFullYear() + '').substr(4 - RegExp.$1.length))
  }
  const o = {
    'M+': date.getMonth() + 1,
    'd+': date.getDate(),
    'h+': date.getHours(),
    'm+': date.getMinutes(),
    's+': date.getSeconds()
  }
  for (const k in o) {
    if (new RegExp(`(${k})`).test(fmt)) {
      const str = o[k] + ''
      fmt = fmt.replace(RegExp.$1, RegExp.$1.length === 1 ? str : padLeftZero(str))
    }
  }
  return fmt
}

function padLeftZero(str) {
  return ('00' + str).substr(str.length)
}
/*
 * 隐藏用户手机号中间四位
 */
export function hidePhone(phone) {
  return phone.replace(/(\d{3})\d{4}(\d{4})/, '$1****$2')
}
/*
 * 时间戳差值转换日期 
 * 格式：YYYY/MM/dd 上午hh:mm:ss
 * timestamp 时间戳
 */
export function toChangeTimestamp(timestamp){
  return new Date(parseInt(timestamp)).toLocaleString()
}
/*
 * 时间戳差值转换日期 
 * 格式：刚刚、几分钟前
 * timestamp 时间戳
 */
export function changeTimestamp(timestamp){
  let minute = 1000 * 60
	let hour = minute * 60
	let day = hour * 24
	let month = day * 30
	let now = new Date().getTime()
	let diffValue = now - parseInt(timestamp) 
	if(diffValue < 0){return}
	let monthC = diffValue / month
	let weekC = diffValue / (7 * day)
	let dayC = diffValue / day
	let hourC = diffValue / hour
	let minC = diffValue / minute
	if(monthC >= 1){
		if(monthC <= 12) {
			var result = "" + parseInt(monthC) + "月前"
		} else {
			result = "" + parseInt(monthC / 12) + "年前"
		}
	}
	else if(weekC >= 1){
		result ="" + parseInt(weekC) + "周前"
	}
	else if(dayC >= 1){
		result =""+ parseInt(dayC) + "天前"
	}
	else if(hourC >= 1){
		result =""+ parseInt(hourC) + "小时前"
	}
	else if(minC >= 1){
		result =""+ parseInt(minC) + "分钟前"
	}else
	result = "刚刚"
	return result
}

