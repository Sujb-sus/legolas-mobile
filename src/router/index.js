import Vue from 'vue'
import Router from 'vue-router'
import NProgress from 'nprogress' // 加载进度条

Vue.use(Router)
export const routes = [{
  path: '/',
  name: 'index',
  redirect: '/home',
  component: () => import('@/views/home/index'), // 路由懒加载
  meta: {
    title: '首页', // 页面标题
    keepAlive: false, // keep-alive 标识
  },
  children: [{
    path: '/about',
    name: 'about',
    component: () => import('@/views/home/about'),
    meta: {
      title: '关于我',
      keepAlive: false
    }
  },
  {
    path: '/activity',
    name: 'activity',
    component: () => import('@/views/home/activity'),
    meta: {
      title: '活动',
      keepAlive: false
    },
  },
  {
    path: '/task',
    name: 'task',
    component: () => import('@/views/home/task'),
    meta: {
      title: '任务',
      keepAlive: false
    },
  },
  {
    path: '/home',
    name: 'home',
    component: () => import('@/views/home/home'),
    meta: {
      title: '首页',
      keepAlive: false
    },
  },
 ]
},
{
  path: '/text',
  name: 'text',
  component: () => import('@/views/text'),
  meta: {
    title: '测试',
    keepAlive: false
  },
},
{
  path: '/login',
  name: 'login',
  component: () => import('@/views/login'),
  meta: {
    title: '登陆',
    keepAlive: false
  },
},
{
  path: '/register',
  name: 'register',
  component: () => import('@/views/register'),
  meta: {
    title: '注册',
    keepAlive: false
  },
},
{
  path: '/forgetPassword',
  name: 'forgetPassword',
  component: () => import('@/views/forgetPassword'),
  meta: {
    title: '忘记密码',
    keepAlive: false
  },
},
{
  path: '/guideDownload',
  name: 'guideDownload',
  component: () => import('@/views/guideDownload'),
  meta: {
    title: '引导页下载',
    keepAlive: false
  },
},
{
  path: '/activity/writeInvite',
  name: 'writeInvite',
  component: () => import('@/views/activity/writeInvite'),
  meta: {
    title: '填写邀请码',
    keepAlive: false
  },
},
{
  path: '/activity/finishInvite',
  name: 'finishInvite',
  component: () => import('@/views/activity/finishInvite'),
  meta: {
    title: '完成填写邀请码',
    keepAlive: false
  },
},
{
  path: '/task/taskDetails/:id',
  name: 'taskDetails',
  component: () => import('@/views/task/taskDetails'),
  meta: {
    title: '任务详情',
    keepAlive: false
  }
},
{
  path: '/about/interactionCenter/:active',
  name: 'interactionCenter',
  component: () => import('@/views/about/interactionCenter'),
  meta: {
    title: '互动中心',
    keepAlive: false
  }
},
{
  path: '/home/details/synthesize/singlePicDetails/:id',
  name: 'singlePicDetails',
  component: () => import('@/views/home/details/synthesize/singlePicDetails'),
  meta: {
    title: '综合版块下单张图片页面详情',
    keepAlive: false
  }
},
{
  path: '/home/details/synthesize/wordPicsDetails/:tabType/:id',
  name: 'wordPicsDetails',
  component: () => import('@/views/home/details/synthesize/wordPicsDetails'),
  meta: {
    title: '综合版块下多张图片页面详情',
    keepAlive: false
  }
},
{
  path: '/home/details/synthesize/onlyWordsDetails/:tabType/:id',
  name: 'onlyWordsDetails',
  component: () => import('@/views/home/details/synthesize/onlyWordsDetails'),
  meta: {
    title: '综合版块下仅有文字页面详情',
    keepAlive: false
  }
},
{
  path: '/home/details/video/singleVideoDetails/:tabType/:id',
  name: 'singleVideoDetails',
  component: () => import('@/views/home/details/video/singleVideoDetails'),
  meta: {
    title: '视频页面详情',
    keepAlive: false
  }
},
{
  path: '/home/details/pics/allPicsDetails/:tabType/:id',
  name: 'allPicsDetails',
  component: () => import('@/views/home/details/pics/allPicsDetails'),
  meta: {
    title: '图片页面详情',
    keepAlive: false
  }
},
{
  path: '/home/details/pics/picComments/:id',
  name: 'picComments',
  component: () => import('@/views/home/details/pics/picComments'),
  meta: {
    title: '图片评论页面',
    keepAlive: false
  }
},
{
  path: '/home/details/forum/hotNews',
  name: 'hotNews',
  component: () => import('@/views/home/details/forum/hotNews'),
  meta: {
    title: '论坛版块的最新消息页面',
    keepAlive: false
  }
},
{
  path: '/home/details/forum/forumItemDetails/:tabType/:id',
  name: 'forumItemDetails',
  component: () => import('@/views/home/details/forum/forumItemDetails'),
  meta: {
    title: '论坛版块下子菜单的详情页面',
    keepAlive: false
  }
},
{
  path: '/about/my/myCollects',
  name: 'myCollects',
  component: () => import('@/views/about/my/myCollects'),
  meta: {
    title: '我的收藏',
    keepAlive: false
  }
},
{
  path: '/about/my/myPosts',
  name: 'myPosts',
  component: () => import('@/views/about/my/myPosts'),
  meta: {
    title: '我的帖子',
    keepAlive: false
  }
},
{
  path: '/about/my/myTeam',
  name: 'myTeam',
  component: () => import('@/views/about/my/myTeam'),
  meta: {
    title: '我的团队',
    keepAlive: false
  }
},
{
  path: '/about/my/myInfo',
  name: 'myInfo',
  component: () => import('@/views/about/my/myInfo'),
  meta: {
    title: '我的信息',
    keepAlive: false
  }
},
{
  path: '/about/my/myPurse',
  name: 'myPurse',
  component: () => import('@/views/about/my/myPurse'),
  meta: {
    title: '我的钱包',
    keepAlive: false
  }
},
{
  path: '/about/my/myRecharge',
  name: 'myRecharge',
  component: () => import('@/views/about/my/myRecharge'),
  meta: {
    title: '我的充值',
    keepAlive: false
  }
},
{
  path: '/about/my/myTasks',
  name: 'myTasks',
  component: () => import('@/views/about/my/myTasks'),
  meta: {
    title: '我的任务',
    keepAlive: false
  }
},
{
  path: '/about/my/myMessages',
  name: 'myMessages',
  component: () => import('@/views/about/my/myMessages'),
  meta: {
    title: '我的消息',
    keepAlive: false
  }
},
{
  path: '/about/my/message/orderMessages',
  name: 'orderMessages',
  component: () => import('@/views/about/my/message/orderMessages'),
  meta: {
    title: '我的接单消息',
    keepAlive: false
  }
},
{
  path: '/about/my/message/taskMessages',
  name: 'taskMessages',
  component: () => import('@/views/about/my/message/taskMessages'),
  meta: {
    title: '我的任务消息', 
    keepAlive: false
  }
},
{
  path: '/about/my/myOrders',
  name: 'myOrders',
  component: () => import('@/views/about/my/myOrders'),
  meta: {
    title: '我的接单',
    keepAlive: false
  }
},
{
  path: '/about/my/browsingHistory',
  name: 'browsingHistory',
  component: () => import('@/views/about/my/browsingHistory'),
  meta: {
    title: '浏览记录',
    keepAlive: false
  }
},
{
  path: '/about/my/myGrowthValue',
  name: 'myGrowthValue',
  component: () => import('@/views/about/my/myGrowthValue'),
  meta: {
    title: '我的成长值',
    keepAlive: false
  }
},
{
  path: '/about/userInfoDetails/:id',
  name: 'userInfoDetails',
  component: () => import('@/views/about/userInfoDetails'),
  meta: {
    title: '用户详情信息',
    keepAlive: false
  }
},
{
  path: '/about/system/systemSetting',
  name: 'systemSetting',
  component: () => import('@/views/about/system/systemSetting'),
  meta: {
    title: '系统设置',
    keepAlive: false
  }
},
{
  path: '/about/system/softwareUpdate',
  name: 'softwareUpdate',
  component: () => import('@/views/about/system/softwareUpdate'),
  meta: {
    title: '论坛更新',
    keepAlive: false
  }
},
{
  path: '/about/exchange/exchangeCenter',
  name: 'exchangeCenter',
  component: () => import('@/views/about/exchange/exchangeCenter'),
  meta: {
    title: '兑换中心',
    keepAlive: false
  }
},
{
  path: '/about/exchange/exchangeDetails/:id',
  name: 'exchangeDetails',
  component: () => import('@/views/about/exchange/exchangeDetails'),
  meta: {
    title: '兑换详情',
    keepAlive: false
  }
},
{
  path: '/about/exchange/exchangeRecords',
  name: 'exchangeRecords',
  component: () => import('@/views/about/exchange/exchangeRecords'),
  meta: {
    title: '兑换记录',
    keepAlive: false
  }
},
]

const createRouter = () =>
  new Router({
    // mode: 'history', // 如果你是 history模式 需要配置vue.config.js publicPath
    // base: '/app/',
    scrollBehavior: () => ({ y: 0 }),
    routes: routes
  })
const router = createRouter()
// router.beforeEach(async (to, from, next) => {
//   NProgress.start()
//   const token = localStorage.getItem('user_Info') ? JSON.parse(localStorage.getItem('user_Info')).token : ''
//   if (to.path === '/') {
//     next()
//   } else {
//     if (token) {
//       if (to.path === '/login') {
//         //自动登录判断
//         next({
//           path: '/'
//         })
//         if (to.meta.title) {
//           document.title = to.meta.title
//         }
//         next()
//       }
//     } else {
//       if (to.path === '/login') {
//         next()
//       } else {
//         next(`/login?redirect=${to.path}`)
//       }
//     }
//   }
// })

router.beforeEach(async (to, from, next) => {
  NProgress.start()
  const token = localStorage.getItem('user_Info') ? JSON.parse(localStorage.getItem('user_Info')).token : ''
  if (to.meta.title) {
    document.title = to.meta.title
  }
  next()
})

router.afterEach(() => {
  NProgress.done()
})
export default router
