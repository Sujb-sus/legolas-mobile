import http from "@/utils/http"

// 检查用户名是否可用
export const CheckName = (userName, params = {}) => {
  return http.post(`/api/Auth/CheckName/${userName}`, params)
}
// 发送验证码，手机号必须为1开头的11位数字
export const SendCAPTCHA = (params = {}) => {
  return http.post('/api/Auth/SendSMS', params)
}
// 验证手机验证码是否正确
export const CheckSMS = (params = {}) => {
  return http.post('/api/Auth/CheckSMS', params)
}
// 更换手机号码
export const ChangePhone = (params = {}) => {
  return http.patch('/api/Auth/ChangePhone', params)
}
// 注册用户
export const Register = (params = {}) => {
  return http.post('/api/Auth/Register', params)
}
// 重设密码
export const ResetUserPassword = (params = {}) => {
  return http.post('/api/Auth/ResetPassword', params)
}
// 登入
export const login = (params = {}) => {
  return http.post('/api/Auth/Login', params)
}
// 登出
export const logout = (params = {}) => {
  return http.delete('/api/Auth/Logout', params)
}