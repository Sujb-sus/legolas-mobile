import http from "@/utils/http"

// 获取用户任务 排序字段：0：默认，全部1.热门 2.最新.3.高价
export const getUserTasksList = (params = {}) => {
  return http.get('/api/Activity/UserTasks', params)
}
// 获取用户任务(根据任务Id)
export const getUserTaskById = (taskId, params = {}) => {
  return http.get(`/api/Activity/UserTasks/${taskId}`, params)
}
// 获取首页轮播图/新闻等 type:1.轮播图，2：新闻
export const getIndexList = (params = {}) => {
  return http.get('/api/Activity/list', params)
}
// 填写邀请码
export const setInviteStatus = (invCode, params = {}) => {
  return http.put(`/api/Activity/SetInviteStatus/${invCode}`, params)
}
// 获取签到状态
export const checkInStatus = (params = {}) => {
  return http.get('/api/Activity/CheckIn', params)
}
// 签到
export const checkIn = (params = {}) => {
  return http.put('/api/Activity/CheckIn', params)
}
// 获取兑换商品列表
export const redeemList = (params = {}) => {
  return http.get('/api/Activity/RedeemItem', params)
}
// 单条商品详情
export const redeemItem = (id, params = {}) => {
  return http.get(`/api/Activity/RedeemItem/${id}`, params)
}
// 当前用户兑换列表
export const exchangeRecords = (params = {}) => {
  return http.get('/api/Activity/Exchange', params)
}
