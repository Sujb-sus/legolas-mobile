import http from "@/utils/http"
// 获取板块列表
export const getCategoryList = (params = {}) => {
  return http.get('/api/BBS/category/list', params)
}
// 获取板块列表（通过tag分组）
export const getCategoryGroupList = (params = {}) => {
  return http.get('/api/BBS/category/grouped', params)
}
// 获取帖子列表
export const categoryIdlist = (categoryId, params) => {
  return http.get(`/api/BBS/topic/list/${categoryId}`, params)
}
// 获取单个频道
export const categoryChannel = (id, params) => {
  return http.get(`/api/BBS/category/item/${id}`, params = {
    params
  })
}
// 获取我的帖子列表
export const myPlacard = (params) => {
  return http.get(`/api/BBS/topic/my`, params)
}
// 获取其它用户帖子列表
export const userPlacard = (userId, params) => {
  return http.get(`/api/BBS/topic/user/${userId}`, params = {
    params
  })
}
// 获取帖子详情
export const detailedPlacard = (params) => {
  return http.get(`/api/BBS/topic/${params}`)
}
// 回复列表
export const replyList = (topicId, params) => {
  return http.get(`/api/BBS/topic/${topicId}/reply/list`, params)
}
// 发布帖子
export const releasePlacard = (params) => {
  return http.post('/api/BBS/topic/post', params)
}
// 修改帖子
export const editPlacard = (topicId, params) => {
  return http.patch(`/api/BBS/topic/${topicId}/edit`, params)
}
//删除帖子
export const deletetPlacard = (topicId, params) => {
  return http.delete(`/api/BBS/topic/${topicId}/deletet`)
}
// 回复
export const replyPlacard = (params) => {
  return http.post('/api/BBS/topic/reply', params)
}
// 获取标签列表
export const tagList = (params) => {
  return http.get('/api/BBS/topic/tag/list', params = {
    params
  })
}
// 点赞/取消赞
export const likePlacard = (params) => {
  return http.post('/api/BBS/topic/like', params)
}
//收藏/取消收藏
export const favoritePlacard = (params) => {
  return http.post(`/api/BBS/topic/favorite?topicId=${params.topicId}`)
}
export const collection = (params) => {
  // 获取收藏帖子列表
  return http.get('/api/BBS/topic/favorite', params = { params })
}
// 举报
export const reportPlacard = (params) => {
  return http.post('/api/BBS/report', params)
}
// 获取音频时长
export const videoTime = (url) => {
  return axios.get(url)
}
