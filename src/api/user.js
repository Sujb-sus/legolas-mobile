import http from "@/utils/http"

// 用户登录
export const handleLogin = (params) => {
  return http.post('/api/Auth/Login', params)
}
// 获取当前用户的基本信息
export const userInfo = () => {
  return http.get('/api/User/Info')
}
// 获取当前用户团队的成员列表
export const leafUsers = () => {
  return http.get('/api/User/LeafUsers')
}
// 获取当前用户的粉丝列表
export const Fans = () => {
  return http.get('/api/User/Fans')
}
// 获取当前用户的关注列表
export const Followeds = () => {
  return http.get('/api/User/Followed')
}
// 获取当前用户的状态
export const userStatus = () => {
  return http.get('/api/User/StatusCount')
}
// 获取交易详情
export const userAccDetails = () => {
  return http.get('/api/User/AccountDetail')
}
// 用户信消息列表(1=消息；2=回复；3=团队；4=任务)
export const userMessages = (params) => {
  return http.get('/api/User/Messages', params)
}
// 用户等级阈值
export const levelThreshold = () => {
  return http.get('/api/User/LevelThreshold')
}
