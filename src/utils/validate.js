/**
 * Created by Sunnie on 19/06/04.
 */

/**
 * @param {string} path
 * @returns {Boolean}
 */
export function isExternal(path) {
  return /^(https?:|mailto:|tel:)/.test(path)
}

/**
 * @param {string} str
 * @returns {Boolean}
 */
export function validUsername(str) {
  const valid_map = ['admin', 'editor']
  return valid_map.indexOf(str.trim()) >= 0
}

/**
 * @param {string} str
 * @returns {Boolean}
 */
export function validPhoneNum(str) {
  const valid_map = /^1[345789]\d{9}$/
  return valid_map.test(str)
}

/**
 * @param {string} str
 * @returns {Boolean}
 */
export function validPassword(str) {
  const valid_map = /^[a-zA-Z0-9]{4,16}$/
  return valid_map.test(str)
}
