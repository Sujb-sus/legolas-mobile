import {
  SET_THEME_COLOR,
  QINIU_IMG
} from "./modules-type"
import { getEntryPoint } from "@/api/file"
const app = {
  namespaced: true,
  state: {
    userName: '',
    baseColor: localStorage.getItem('theme-color') ? localStorage.getItem('theme-color') : '#4B84E6',
    arr: [],
    imgUrl: localStorage.getItem('ImgUrl') ? localStorage.getItem('ImgUrl') : ''
  },
  mutations: {
    [SET_THEME_COLOR](state, params) {
      state.baseColor = params
    },
    [QINIU_IMG](state, url) {
      state.imgUrl = url
    }
  },
  actions: {
    setThemeColor({
      commit
    }, params) {
      commit(SET_THEME_COLOR, params)
      localStorage.setItem('theme-color', params)
    },

    async handeleQiNiu({ commit }) {
      const res = await getEntryPoint()
      if (res && res.code === 200) {
        const url = res.result.ossPrefix
        localStorage.setItem('ImgUrl', url)
        commit(QINIU_IMG, url)
      }
    }
  }
}

export default app
