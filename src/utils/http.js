import axios from 'axios';
//设置请求头
const baseURL = process.env.VUE_APP_API;
// const baseURL = '/';
// console.log(baseURL)
// localStorage.setItem("token", 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjIiLCJuYSI6InhpYW94dWUiLCJybmEiOiLml6Dmg4Xpm6oiLCJuYmYiOjE1ODkzNTQzOTksImV4cCI6MTU4OTM2MTU5OSwiaXNzIjoiaHR0cDovL3d3dy56aG9udGFpLmNvbSIsImF1ZCI6Imh0dHA6Ly93d3cuemhvbnRhaS5jb20ifQ.9JwFdxIbDRDYpy60Aaiy6K6z9f-TAmwYr_LD3amflro')
axios.interceptors.request.use(
  config => {
    const token = localStorage.getItem('user_Info') ? JSON.parse(localStorage.getItem('user_Info')).token : '';
    if (token) {
      config.headers.Authorization = 'Bearer ' + token;
    }
    return config;
  },
  err => {
    return Promise.reject(err);
  }
);

axios.interceptors.response.use(
  response => {
    return response;
  },
  error => {
    return Promise.resolve(error.response);
  }
);

function checkStatus(response) {
  // console.log(response)
  // loading
  // 如果http状态码正常，则直接返回数据
  if (response && (response.status === 200 || response.status === 304 || response.status === 400)) {
    return response.data;
    // 如果不需要除了data之外的数据，可以直接 return response.data
  }
  // 异常状态下，把错误信息返回去
  return {
    status: -404,
    msg: '网络异常'
  };
}

function checkCode(res) {
  // 如果code异常(这里已经包括网络错误，服务器错误，后端抛出的错误)，可以弹出一个错误提示，告诉用户
  if (res.status === -404) { }
  if (res.data && !res.data.success) { }
  return res;
}

export default {
  post(url, data) {
    return axios({
      method: 'post',
      baseURL,
      url,
      data: data,
      timeout: 10000,
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Content-Type': 'application/json; charset=UTF-8'
      }
    })
      .then(response => {
        return checkStatus(response);
      })
      .then(res => {
        return checkCode(res);
      });
  },
  get(url, params) {
    return axios({
      method: 'get',
      baseURL,
      url,
      params, // get 请求时带的参数
      timeout: 10000,
      headers: {
        'X-Requested-With': 'XMLHttpRequest'
      }
    })
      .then(response => {
        return checkStatus(response);
      })
      .then(res => {
        return checkCode(res);
      });
  },
  put(url, data) {
    return axios({
      method: 'put',
      baseURL,
      url,
      data: data,
      timeout: 10000,
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Content-Type': 'application/json; charset=UTF-8'
      }
    })
      .then(response => {
        return checkStatus(response);
      })
      .then(res => {
        return checkCode(res);
      });
  },
  delete(url, data) {
    return axios({
      method: 'delete',
      baseURL,
      url,
      data: data,
      timeout: 10000,
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Content-Type': 'application/json; charset=UTF-8'
      }
    })
      .then(response => {
        return checkStatus(response);
      })
      .then(res => {
        return checkCode(res);
      });
  }
};
