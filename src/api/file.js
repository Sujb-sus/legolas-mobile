import http from "@/utils/http"

// 获取七牛云域名
export const getEntryPoint = () => {
  return http.get("/api/Configs")
}
// 检查App是否有新版本
export const checkVersion = (params) => {
  return http.get("/api/File/CheckVersion", params)
}
