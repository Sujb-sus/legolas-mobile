export const SET_USER_NAME = 'SET_USER_NAME'
export const HOMESWIPER = "HOMESWIPER"
export const USER_INFO = "user_info"
export const USER_STATUS = "user_status"
export const GET_TOKEN = "get_token"
export const SET_THEME_COLOR = "set_theme_color"
export const QINIU_IMG = 'qiniu_img'// 获取七牛服务器域名
